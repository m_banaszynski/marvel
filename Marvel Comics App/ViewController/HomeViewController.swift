//
//  HomeViewController.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 08/07/2021.
//

import UIKit
import Combine
import CryptoKit
import Kingfisher

class HomeViewController: UIViewController {
    
    var fetchData = FetchData()
    var fetchComics: [Comics] = []
    {
        didSet{
            DispatchQueue.main.async {
                self.homeTableView.reloadData()
                self.homeTableView.isHidden = false
            }
        }
    }
    
    @IBOutlet weak var homeTableView: UITableView!
    
    func setHomeTableView(){
        self.homeTableView.register(UINib(nibName: ComicTableViewCell.identifier, bundle: nil), forCellReuseIdentifier:ComicTableViewCell.identifier)
        
        homeTableView.delegate = self
        homeTableView.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData.fetchComics{ fetchedComics in
            self.fetchComics = fetchedComics
        }
        
        self.setHomeTableView()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchComics.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ComicTableViewCell.identifier, for: indexPath) as? ComicTableViewCell
        else{
            return UITableViewCell()
        }
        
        let comics: Comics = fetchComics[indexPath.item]

        cell.title?.text = comics.title
        cell.creators?.text = "written by: \(getCreators(data: comics))"
        cell.desc?.text = comics.description
        cell.thumbnail?.kf.setImage(with: extractImage(data: comics.thumbnail))
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let view = DetailsViewController()
        view.comics=fetchComics[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewControllerID") as! DetailsViewController
        vc.comics = fetchComics[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

func extractImage(data: [String: String]) -> URL{
    
    let path = data["path"] ?? ""
    let ext = data["extension"] ?? ""
    
    var url = "\(path).\(ext)"
    
    url.insert("s", at: url.index(url.startIndex, offsetBy: 4))
    
    return URL(string: url)!
}

func getCreators(data: Comics) -> String{
    
    var creators = ""
    for comic in data.creators.items{
        creators += "\(comic.name) "
    }
    return creators
}

func MD5(data: String) -> String{
    
    let hash = Insecure.MD5.hash(data: data.data(using: .utf8) ?? Data())
    
    return hash.map{
        String(format: "%02hhx", $0)
    }
    .joined()
}

