//
//  DetailsViewController.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 09/07/2021.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {

    @IBOutlet var detailsScrollView: UIScrollView!
    @IBOutlet var comicsTitle: UILabel!
    @IBOutlet var comicsCreators: UILabel!
    @IBOutlet var comicsDescription: UILabel!
    @IBOutlet weak var comicsThumbnail: UIImageView!
    
    var comics: Comics?
    
    func setComicsDetails(){
        self.detailsScrollView.isDirectionalLockEnabled = true
        self.comicsTitle.text=comics?.title
        self.comicsCreators.text = "written by \(getCreators(data: comics!))"
        self.comicsDescription.text = comics?.description
        self.comicsThumbnail.kf.setImage(with: extractImage(data: comics!.thumbnail))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setComicsDetails()
        }
 
    
    func getCreators(data: Comics) -> String{

        var creators = ""
        for comic in data.creators.items{
            creators += "\(comic.name) "
        }
        return creators
    }
    
}




