//
//  SearchViewController.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 08/07/2021.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchTableView: UITableView!
    
    
    var fetchData = FetchData()
    var fetchComics: [Comics] = []
    var filteredComics: [Comics]!
    
    func setSearchTableView(){
        self.searchTableView.register(UINib(nibName: ComicTableViewCell.identifier, bundle: nil), forCellReuseIdentifier:ComicTableViewCell.identifier)
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
    }
    
    override func viewDidLoad() {

        fetchData.fetchComics{ fetchedComics in
            self.fetchComics = fetchedComics
        }
        
        super.viewDidLoad()
        
        self.setSearchTableView()
        searchBar.delegate = self
        
        filteredComics = []

    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return filteredComics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ComicTableViewCell.identifier, for: indexPath) as? ComicTableViewCell
        else{
            return UITableViewCell()
        }
        
        var comics: Comics = filteredComics[indexPath.row]

        cell.title?.text = comics.title
        cell.creators?.text = "written by: \(getCreators(data: comics))"
        cell.desc?.text = comics.description
        cell.thumbnail?.kf.setImage(with: extractImage(data: comics.thumbnail))
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let view = DetailsViewController()
        view.comics=filteredComics[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewControllerID") as! DetailsViewController
        vc.comics = filteredComics[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        filteredComics = []
        
        if searchText == "" {
            
            self.searchTableView.isHidden=true
            filteredComics = []
        }
        else {
            for comics in fetchComics {
                if comics.title.lowercased().contains(searchText.lowercased())
                {
                    filteredComics.append(comics)
                }
            }
        }
        
        
        self.searchTableView.isHidden=false
        self.searchTableView.reloadData()
    }
}
