//
//  ComicTableViewCell.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 08/07/2021.
//

import UIKit

class ComicTableViewCell: UITableViewCell {

    static let identifier = "ComicTableViewCell"
    
    @IBOutlet var title :UILabel!
    @IBOutlet var creators :UILabel!
    @IBOutlet var desc :UILabel!
    @IBOutlet var thumbnail :UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
