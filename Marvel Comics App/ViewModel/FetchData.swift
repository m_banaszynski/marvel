//
//  FetchData.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 11/07/2021.
//

import Foundation

class FetchData{
    func fetchComics(completionHandler:@escaping ([Comics]) -> Void){
        
        var fetchComics: [Comics] = []
        
        let url="https://gateway.marvel.com/v1/public/comics?ts=1&apikey=080a502746c8a60aeab043387a56eef0&hash=6edc18ab1a954d230c1f03c590d469d2&limit=25&offset=25&orderBy=-onsaleDate"
        
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: URL(string: url)!) { (data, _, err) in
           
            if let error = err{
                print(error.localizedDescription)
                return
            }
            
            guard let APIData = data else{
                print("no data found")
                return
            }
            
            do{
                
                let comics = try JSONDecoder().decode(APIResult.self, from: APIData)
                
                fetchComics.append(contentsOf: comics.data.results)
                completionHandler(fetchComics)
                
    //                DispatchQueue.main.async {
    //                    HomeViewController.fetchedComics.append(contentsOf: comics.data.results)
    //                }
                
            }
            catch{
                print(error.localizedDescription)
            }
        }
        .resume()
    }
}
