//
//  Comics.swift
//  Marvel Comics App
//
//  Created by Maciej Banaszyński on 08/07/2021.
//

struct APIResult: Codable{
    var data: APIComicsData
}

struct APIComicsData: Codable{
    var count: Int
    var results: [Comics]
}

struct CreatorList:Codable{
    var items: [CreatorSummary]
    
}

struct CreatorSummary: Codable{
    var name: String
}

struct Comics: Identifiable,Codable{
    var id: Int
    var title: String
    var creators: CreatorList
    var description: String?
    var thumbnail: [String: String]
    var urls: [[String: String]]
}
